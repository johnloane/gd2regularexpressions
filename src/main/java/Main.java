import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
    public static void main(String[] args)
    {
        String string = "I am a string";
        System.out.println(string);
        String yourString = string.replaceAll("I am", "You are");
        System.out.println(yourString);

        String alphanumeric = "abcDeeeF12Ghhiiiiijk99z";
        System.out.println(alphanumeric.replaceAll(".", "Y"));

        System.out.println(alphanumeric.replaceAll("^abcDeee" , "YYY"));

        String secondString = "abcDeeeF12abcDeeeGhhiiiiijk99z";
        System.out.println(secondString.replaceAll("^abcDeee" , "YYY"));

        System.out.println(alphanumeric.matches("^hello"));
        System.out.println(alphanumeric.matches("^abcDeee"));
        System.out.println(alphanumeric.matches("abcDeeeF12Ghhiiiiijk99z"));

        System.out.println(alphanumeric.replaceAll("ijk99z$", "The end"));
        System.out.println(alphanumeric.replaceAll("[aei]", "X"));
        System.out.println(alphanumeric);
        System.out.println(alphanumeric.replaceAll("[aei][Fj]", "X"));

        System.out.println("harry".replaceAll("[Hh]arry", "Harry"));

        String newAlphanumeric = "abcDeeeF12Ghhiiiiijk99z";
        System.out.println(newAlphanumeric);
        System.out.println(newAlphanumeric.replaceAll("[^ej]", "X"));
        System.out.println(newAlphanumeric);
        System.out.println(newAlphanumeric.replaceAll("[abcdef345678]", "X"));
        System.out.println(newAlphanumeric.replaceAll("[a-fA-F3-8]", "X"));
        System.out.println(newAlphanumeric.replaceAll("(?i)[a-f3-8]", "X"));
        System.out.println(newAlphanumeric.replaceAll("[0-9]", "X"));
        System.out.println(newAlphanumeric.replaceAll("\\d", "X"));
        System.out.println(newAlphanumeric.replaceAll("\\D", "X"));

        String hasWhitespace = "I have blanks and \ta tab, and also a newline\n";
        System.out.println(hasWhitespace);
        System.out.println(hasWhitespace.replaceAll("\\s", ""));
        System.out.println(hasWhitespace.replaceAll("\t", ""));
        System.out.println(hasWhitespace.replaceAll("\\S", "X"));
        System.out.println(hasWhitespace.replaceAll("\\w", "X"));
        System.out.println(hasWhitespace.replaceAll("\\b", "X"));

        String thirdAlphanumeric = "abcDeF12Ghhiiiijkl99z";
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe{4}", "X"));
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe+", "X"));
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe*", "X"));

        System.out.println(thirdAlphanumeric.replaceAll("^abcDe{2,5}", "X"));

        System.out.println(thirdAlphanumeric.replaceAll("h+i*j", "Y"));

        StringBuilder htmlText = new StringBuilder("<h1>My favourite games</h1>");
        htmlText.append("<h2>Space Invaders</h2>");
        htmlText.append("<p>OMG this is the best game ever</p>");
        htmlText.append("<h2>Asteroids</h2>");
        htmlText.append("<p>FTL what an amazing game</p>");
        htmlText.append("<h2>Splatterhouse</h2>");
        htmlText.append("<p>Homework check this game out</p>");

        String h2Pattern = "<h2>";
        Pattern pattern = Pattern.compile(h2Pattern);
        Matcher matcher = pattern.matcher(htmlText);
        System.out.println(matcher.matches());

        matcher.reset();
        int count = 0;
        while(matcher.find())
        {
            count++;
            System.out.println("Occurrence " + count + " : " + matcher.start() + " to " + matcher.end());
        }

        String h2GroupPattern = "(<h2>.*?</h2>)";
        Pattern groupPattern = Pattern.compile(h2GroupPattern);
        Matcher groupMatcher = groupPattern.matcher(htmlText);
        System.out.println(groupMatcher.matches());

        groupMatcher.reset();
        while(groupMatcher.find())
        {
            System.out.println("Occurrence: " + groupMatcher.group(1));
        }

        String h2TextGroups = "(<h2>)(.+?)(</h2>)";
        Pattern h2TextPattern = Pattern.compile(h2TextGroups);
        Matcher h2TextMatcher = h2TextPattern.matcher(htmlText);

        while(h2TextMatcher.find())
        {
            System.out.println("Occurrence: " + h2TextMatcher.group(2));
        }

        System.out.println("harry".replaceAll("[H|h]arry", "Larry"));

        String tvTest = "tstvtkt";
        //Find all t's that are not followed by v's
        String tNotVRegExp = "t(?!v)";
        Pattern tNotVPattern = Pattern.compile(tNotVRegExp);
        Matcher tNotVMatcher = tNotVPattern.matcher(tvTest);

        count = 0;
        while(tNotVMatcher.find())
        {
            count++;
            System.out.println("Occurrence " + count + " : " + tNotVMatcher.start() + " to " + tNotVMatcher.end());
        }

        //testUSPhoneRegex();
//        testIrishMobileRegex();
//        testVisaCards();
//        challenge1();
//        challenge2();
//        challenge3();
//        challenge4();
//        challenge5();
//        challenge6();
//        challenge7();
//        challenge8();
//        challenge9();
        //challenge10();
        //challenge12();
        challenge16();
    }

    public static void challenge1()
    {
        /* Write regex to match "I want an Alienware Area 51m for Christmas"*/
        String challenge1 = "I want an Alienware Area 51m for Christmas";
        System.out.println(challenge1.matches("I want an Alienware Area 51m for Christmas"));

    }

    public static void challenge2()
    {
        //Write regex to match either "I want a computer for Christmas" or "I want a dog for Christmas"
        String regExp = "^(I want a )((computer)|(dog))( for Christmas)$";
        String test1 = "I want a computer for Christmas";
        String test2 = "I want a dog for Christmas";

        System.out.println(test1.matches(regExp));
        System.out.println(test2.matches(regExp));
    }

    public static void challenge3()
    {
        //Do the same as challenge2 but use Pattern and Matcher classes
        String test1 = "I want a computer for Christmas";
        String test2 = "I want a dog for Christmas";
        String regExp = "^(I want a )((computer)|(dog))( for Christmas)$";

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(test1);
        System.out.println(matcher.matches());

        Matcher matcher2 = pattern.matcher(test2);
        System.out.println(matcher2.matches());


    }

    public static void challenge4()
    {
        // In the following sentence "Replace all blanks with underscores"
        String challengeString = "Replace all blanks with underscores";
        System.out.println(challengeString.replaceAll("\\s", "_"));
    }

    public static void challenge5()
    {
        //"aaabccccccccdddefffg"
        //Write the shortest regular expression you can to match this string
        String challenge5 = "aaabccccccccdddefffg";
        System.out.println(challenge5.matches("[a-g]+"));
    }

    public static void challenge6()
    {
        //Write regex for exact match
        String challenge6 = "aaabccccccccdddefffg";
        System.out.println(challenge6.matches("^a{3}bc{8}d{3}ef{3}g$"));
    }

    public static void challenge7()
    {
        //Allow any number of upper or lowercase letters followed by a . followed by any number of digits
        String challenge7 = "abcd.135";
        String regExp = "^[A-z][a-z]+\\.\\d+$";

        System.out.println(challenge7.matches(regExp));
    }

    public static void challenge8()
    {
        //Use Pattern and Matcher classes and regex to identify and print each of the numbers in the string below
        String challenge8 = "abcd.135uvqz.7tzik.999";
        String regExp = "(\\d+)";

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(challenge8);
        while(matcher.find())
        {
            System.out.println("Occurrence: " + matcher.group(1));
        }


    }

    public static void challenge9()
    {
        String challenge9 = "abcd.135\tuvqz.7\ttzik.999\n";
        //Write a regex to print all of the numbers
        String regExp = "(\\d+)";

        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(challenge9);
        while(matcher.find())
        {
            System.out.println("Occurrence: " + matcher.group(1));
        }
    }

    public static void challenge10()
    {
        //Print the start and end of each occurrence of numbers
        String challenge10 = "abcd.135\tuvqz.7\ttzik.999\n";
        Pattern pattern = Pattern.compile("[A-Za-z]+\\.(\\d+)\\s");
        Matcher matcher = pattern.matcher(challenge10);
        while(matcher.find())
        {
            System.out.println("Occurrence: start = " + matcher.start(1) + " end = " + (matcher.end(1)-1));
        }
    }

    public static void challenge11()
    {
        String challenge11 = "{0,2}, {0, 5}, {1,3}, {2,4}";
        //Pull out the numbers minus the curly brackets
        //Occurrence 0, 2
        //Occurrence 0, 5
        Pattern pattern = Pattern.compile("\\{(.+?)\\}");
        Matcher matcher = pattern.matcher(challenge11);
        while(matcher.find())
        {
            System.out.println("Occurrence: " + matcher.group(1));
        }
    }

    public static void challenge12()
    {
        String challenge12 = "{0,2}, {0, 5}, {1,3}, {2,4} {x,y}, {6, 34}";
        //Same as challenge 11 but ensure that the entries are digits
        Pattern pattern = Pattern.compile("\\{(\\d+,\\s?\\d+)\\}");
        Matcher matcher = pattern.matcher(challenge12);
        while(matcher.find())
        {
            System.out.println("Occurrence: " + matcher.group(1));
        }
    }

    public static void challenge13()
    {
        String challenge13 = "11111";
        System.out.println(challenge13.matches("^(\\d{5})$"));
    }

    public static void challenge14()
    {
        String challenge14 = "11111-1111";
        System.out.println(challenge14.matches("^(\\d{5}-\\d{4})$"));
    }

    public static void challenge15()
    {
        String challenge13 = "11111";
        String challenge14 = "11111-1111";
        //Write regex that will match either of the above
        System.out.println(challenge14.matches("^(\\d{5}(-\\d{4})?)$"));
        System.out.println(challenge13.matches("^(\\d{5}(-\\d{4})?)$"));
    }

    public static void challenge16()
    {
        //Need a regular expression for Eircode
        //7 characters first is a letter, next two are digits except for D6W
        //last 4 are characters
        //e.g A65 F4E2
        String challenge16 = "A65 F4E2";
        String challenge16exception = "D6W 1234";
        //Same as challenge 11 but ensure that the entries are digits
        Pattern pattern = Pattern.compile("^([ACDEFHKNPRTVWXY]{1}[0-9]{1}[0-9W]{1}[\\ \\-]?[0-9ACDEFHKNPRTVWXY]{4})$");
        Matcher matcher = pattern.matcher(challenge16exception);
        System.out.println(matcher.matches());
    }


    public static void testVisaCards()
    {
        String visa1 = "4444444444444";
        String visa2 = "5444444444444";
        String visa3 = "4444444444444444";
        String visa4 = "4444";

        System.out.println("visa1 = " + visa1.matches("^(4[0-9]{12}([0-9]{3})?)$"));
        System.out.println("visa2 = " + visa2.matches("^(4[0-9]{12}([0-9]{3})?)$"));
        System.out.println("visa3 = " + visa3.matches("^(4[0-9]{12}([0-9]{3})?)$"));
        System.out.println("visa4 = " + visa4.matches("^(4[0-9]{12}([0-9]{3})?)$"));

    }

    public static void testUSPhoneRegex()
    {
        String phone1 = "1234567890"; //Fail no brackets, no space
        String phone2 = "(123) 456-7890"; //Pass
        String phone3 = "123 456-7890"; //Fail no brackets
        String phone4 = "(123)456-7890"; //Fail no space

        System.out.println("phone4 = " + phone4.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
    }

    public static void testIrishMobileRegex()
    {
        //+3538[3,5,6,7,8,9][0-9]{7}
        //Homework complete this
        String phone1 = "+353861234567";
        String phone2 = "1234567890";
        String phone3 = "+353881234567";
        String phone4 = "353891234567";
        String phone5 = "+343861234567";
        String phone6 = "+35386123456";
        String phone7 = "+3538612345677";
        System.out.println("phone1 = " + phone1.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
        System.out.println("phone2 = " + phone2.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
        System.out.println("phone3 = " + phone3.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
        System.out.println("phone4 = " + phone4.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
        System.out.println("phone5 = " + phone5.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
        System.out.println("phone6 = " + phone6.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
        System.out.println("phone7 = " + phone7.matches("^([\\+]{1}3538[3,5,6,7,9][0-9]{7})$"));
    }
}
